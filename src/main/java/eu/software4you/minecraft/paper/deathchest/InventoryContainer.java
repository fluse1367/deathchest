package eu.software4you.minecraft.paper.deathchest;

import eu.software4you.configuration.yaml.ExtYamlSub;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Getter
public class InventoryContainer {
    private final Block block;
    private final UUID owner;
    private final String ownerName;
    private final Inventory inv;
    private final long created;
    @Setter
    private ConfigurationSection section;

    public InventoryContainer(Block block, Player owner, Inventory inv) {
        this.block = block;
        this.owner = owner.getUniqueId();
        this.ownerName = owner.getName();
        this.inv = inv;
        this.created = System.currentTimeMillis();
    }

    public InventoryContainer(Block block, UUID owner, String ownerName, Inventory inv, long created, ConfigurationSection section) {
        this.block = block;
        this.owner = owner;
        this.ownerName = ownerName;
        this.inv = inv;
        this.created = created;
        this.section = section;
    }

    public Location getLocation() {
        return block.getLocation();
    }

    public boolean publiclyAvailable() {
        // return if 60 minutes have passed
        val c = conf();
        int val = c.int32("chest-lock.time", 60);
        if (val < 0)
            return false;
        TimeUnit unit = TimeUnit.MINUTES;
        try {
            unit = TimeUnit.valueOf(c.string("chest-lock.unit", "minutes").toUpperCase());
        } catch (IllegalArgumentException e) {
            // ignored
        }
        return created + unit.toMillis(val) < System.currentTimeMillis();
    }

    public boolean hasAccess(Player p) {
        if (!p.isOp() && !p.hasPermission(conf().string("chest-lock-override-permission", "deathcheat.accessothers"))
                && !owner.equals(p.getUniqueId())) {
            return publiclyAvailable();
        }
        return true;
    }

    public void close() {
        val soundLoc = block.getLocation().clone().add(.5, 0, .5);
        new ArrayList<>(inv.getViewers()).forEach(ent -> {
            ent.closeInventory();
            if (ent instanceof Player) {
                ((Player) ent).playSound(soundLoc,
                        Sound.BLOCK_CHEST_CLOSE, SoundCategory.BLOCKS, .25f, 1f);
            }
        });
    }

    public void updateContents() {
        section.set("contents", Arrays.stream(inv.getContents())
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
    }

    private ExtYamlSub conf() {
        return DeathChest.getPlugin(DeathChest.class).getConf();
    }
}
