package eu.software4you.minecraft.paper.deathchest.handler;

import eu.software4you.minecraft.paper.deathchest.DeathChest;
import eu.software4you.minecraft.paper.deathchest.InventoryContainer;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class DataHandler {
    private final File file;
    private final YamlConfiguration yaml;
    @Getter
    private final Map<Location, InventoryContainer> invs = new HashMap<>();

    @SneakyThrows
    public DataHandler(File file) {
        this.file = file;
        if (!file.exists()) {
            val parent = file.getParentFile();
            if (!parent.exists()) {
                if (!parent.mkdirs()) {
                    throw new IllegalStateException("Could not create dir " + parent);
                }
            }

            if (!file.createNewFile()) {
                throw new IllegalStateException("Could not create file " + file);
            }
        }
        yaml = YamlConfiguration.loadConfiguration(file);
    }

    public Optional<InventoryContainer> lookup(Inventory inventory) {
        return invs.values().stream()
                .filter(inventoryContainer -> inventoryContainer.getInv() == inventory)
                .findFirst();
    }

    public Optional<InventoryContainer> lookup(Location loc) {
        if (invs.containsKey(loc))
            return Optional.of(invs.get(loc));

        // attempt to load inventory
        val ic = loadInventory(loc);
        if (ic == null)
            return Optional.empty();

        // put inv into map
        invs.put(loc, ic);

        return Optional.of(ic);
    }

    public void setInventory(Location loc, InventoryContainer inv) {
        String key = toKey(loc);
        ConfigurationSection sec = yaml.createSection(key);
        inv.setSection(sec);
        sec.set("owner", inv.getOwner().toString());
        sec.set("ownerName", inv.getOwnerName());
        sec.set("size", inv.getInv().getSize());
        sec.set("created", inv.getCreated());
        inv.updateContents();
    }

    public void delInventory(Location loc) {
        Optional.ofNullable(invs.remove(loc)).ifPresent(ic -> ic.setSection(null));
        yaml.set(toKey(loc), null);
    }

    @SneakyThrows
    public void save() {
        yaml.save(file);
    }

    private InventoryContainer loadInventory(Location loc) {
        String key = toKey(loc);

        if (!yaml.isConfigurationSection(key))
            return null;

        ConfigurationSection sec = yaml.getConfigurationSection(key);

        UUID owner = UUID.fromString(sec.getString("owner"));
        String ownerName = sec.getString("ownerName");
        int size = sec.getInt(".size");
        val items = sec.getList(".contents");
        long created = sec.getLong("created");

        Inventory inv = Bukkit.createInventory(null, size, DeathChest.getPlugin(DeathChest.class)
                .getLayout().string("chest-title", "%s's Inventory", ownerName));
        inv.setContents(items.toArray(new ItemStack[0]));

        return new InventoryContainer(loc.getBlock(), owner, ownerName, inv, created, sec);
    }

    private String toKey(Location loc) {
        return String.format("%s.%d,%d,%d", loc.getWorld().getName(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
    }
}
