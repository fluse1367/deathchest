package eu.software4you.minecraft.paper.deathchest;

import com.mojang.authlib.GameProfile;
import eu.software4you.minecraft.paper.deathchest.handler.DataHandler;
import eu.software4you.minecraft.paper.deathchest.handler.Handler;
import eu.software4you.minecraft.paper.deathchest.util.ItemUtil;
import eu.software4you.spigot.plugin.ExtendedJavaPlugin;

import java.io.File;
import java.util.UUID;

public class DeathChest extends ExtendedJavaPlugin {
    public static final GameProfile CHEST_PROFILE = ItemUtil.genSkullProfile(
            UUID.fromString("d4607dd9-e26a-432a-b9c2-cfef8a82e90a"),
            "http://textures.minecraft.net/texture/d5c6dc2bbf51c36cfc7714585a6a5683ef2b14d47d8ff714654a893f5da622");

    private DataHandler dataHandler;
    private Handler handler;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        dataHandler = new DataHandler(new File(getDataFolder(), "invs.yml"));
        handler = new Handler(dataHandler, getLayout());
        registerEvents(handler);
    }

    @Override
    public void onDisable() {
        dataHandler.getInvs().values().forEach(InventoryContainer::close);
        dataHandler.save();
    }
}

