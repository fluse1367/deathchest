package eu.software4you.minecraft.paper.deathchest.handler;

import com.mojang.authlib.GameProfile;
import eu.software4you.minecraft.paper.deathchest.DeathChest;
import eu.software4you.minecraft.paper.deathchest.InventoryContainer;
import eu.software4you.reflect.Parameter;
import eu.software4you.reflect.ReflectUtil;
import eu.software4you.spigot.plugin.Layout;
import lombok.val;
import lombok.var;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Handler implements Listener {
    private final DataHandler data;
    private final Layout layout;

    public Handler(DataHandler data, Layout layout) {
        this.data = data;
        this.layout = layout;
    }

    // handle death
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerDeathEvent e) {
        if (e.getDrops().isEmpty() || e.getKeepInventory())
            return;

        // check if block spawning is even possible
        var block = e.getEntity().getLocation().getBlock();
        val loc = block.getLocation();
        if (block.getType() != Material.AIR) {
            var b = block;
            if ((b = loc.clone().add(0, 1, 0).getBlock()).getType() == Material.AIR) {
                block = b;
            } else if ((b = loc.clone().add(1, 0, 0).getBlock()).getType() == Material.AIR) {
                block = b;
            } else if ((b = loc.clone().add(-1, 0, 0).getBlock()).getType() == Material.AIR) {
                block = b;
            } else if ((b = loc.clone().add(0, 0, 1).getBlock()).getType() == Material.AIR) {
                block = b;
            } else if ((b = loc.clone().add(0, 0, -1).getBlock()).getType() == Material.AIR) {
                block = b;
            } else if ((b = loc.clone().add(0, -1, 0).getBlock()).getType() == Material.AIR) {
                block = b;
            } else {
                // block spawning is not possible, abort
                return;
            }
        }
        // fetch drops
        List<ItemStack> drops = new ArrayList<>(e.getDrops());
        e.getDrops().clear();

        // set block as chest player head
        block.setType(Material.PLAYER_HEAD);
        Skull skull = (Skull) block.getState();
        ReflectUtil.forceCall(skull.getClass(), skull, "profile",
                Parameter.single(GameProfile.class, DeathChest.CHEST_PROFILE));
        skull.update(true, true);

        // create inventory
        String title = layout.string("chest-title", "%s's Inventory", e.getEntity().getName());
        int invSize = Math.min((drops.size() / 9) + ((drops.size() % 9) > 0 ? 1 : 0), 6) * 9;
        Inventory inv = Bukkit.createInventory(null, invSize, title);
        inv.setContents(drops.toArray(new ItemStack[0]));

        InventoryContainer ic = new InventoryContainer(block, e.getEntity(), inv);

        // save inventory
        data.setInventory(block.getLocation(), ic);
        data.save();
    }

    // handle chest break
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(BlockBreakEvent e) {
        switch (broken(e.getBlock(), e.getPlayer())) {
            case 0:
                e.setDropItems(false);
                return;
            case 1:
                // no permissions
                e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                        new TextComponent(layout.string("chest-lock", "§cThis chest is magically locked.")));
                e.setCancelled(true);
                return;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(BlockPistonExtendEvent e) {
        if (e.getBlocks().stream()
                .map(Block::getLocation)
                .map(data::lookup)
                .anyMatch(Optional::isPresent)) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(BlockFromToEvent e) {
        if (data.lookup(e.getToBlock().getLocation()).isPresent())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void handle(EntityExplodeEvent e) {
        e.blockList().removeIf(block -> {
            switch (broken(block, null)) {
                case 0:
                    block.setType(Material.AIR);
                case 1:
                    return true;
                default:
                    return false;
            }
        });
    }


    /**
     * @return 0 on successful break, 1 on insufficient permissions, 2 on no inventory
     */
    private int broken(Block block, Player breaker) {
        val loc = block.getLocation();
        val op = data.lookup(loc);
        if (!op.isPresent())
            return 2;
        val ic = op.get();

        if (breaker == null) {
            if (!ic.publiclyAvailable()) {
                return 1;
            }
        } else if (!ic.hasAccess(breaker)) {
            return 1;
        }

        val inv = ic.getInv();
        ic.close();

        // delete inventory
        data.delInventory(loc);
        data.save();


        // spawn items
        val world = loc.getWorld();

        for (ItemStack is : inv.getContents()) {
            if (is != null) {
                world.dropItemNaturally(loc, is);
            }
        }

        return 0;
    }

    // inventory open
    @EventHandler
    public void handle(PlayerInteractEvent e) {
        if (e.getHand() != EquipmentSlot.HAND || e.getAction() != Action.RIGHT_CLICK_BLOCK || e.getPlayer().isSneaking())
            return;
        val block = e.getClickedBlock();
        if (block == null)
            return;
        val loc = block.getLocation();
        val op = data.lookup(loc);
        if (!op.isPresent())
            return;
        val ic = op.get();

        if (!ic.hasAccess(e.getPlayer())) {
            e.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR,
                    new TextComponent(layout.string("chest-lock", "§cThis chest is magically locked.")));
            return;
        }

        // open inventory
        e.setCancelled(true);
        e.getPlayer().openInventory(ic.getInv());
        e.getPlayer().playSound(block.getLocation().clone().add(.5, 0, .5),
                Sound.BLOCK_CHEST_OPEN, SoundCategory.BLOCKS, .25f, 1f);
    }

    // inventory close
    @EventHandler
    public void handle(InventoryCloseEvent e) {
        val op = data.lookup(e.getInventory());
        if (!op.isPresent())
            return;
        val loc = op.get().getLocation();
        val inv = e.getInventory();

        if (e.getPlayer() instanceof Player) {
            ((Player) e.getPlayer()).playSound(loc.clone().add(.5, 0, .5),
                    Sound.BLOCK_CHEST_CLOSE, SoundCategory.BLOCKS, .25f, 1f);
        }

        if (inv.isEmpty()) {
            // delete inventory
            data.delInventory(loc);

            // close other views
            op.get().close();

            // simulate block break
            loc.getWorld().spawnParticle(Particle.BLOCK_CRACK, loc.clone().add(.5, .5, .5), 10, loc.getBlock().getBlockData());
            loc.getWorld().playSound(loc.clone().add(.5, 0, .5),
                    Sound.BLOCK_STONE_BREAK, SoundCategory.BLOCKS, 1, 1);
            loc.getBlock().setType(Material.AIR, true);
        } else {
            // update saved inventory
            op.get().updateContents();
        }
        data.save();
    }

    // inventory modify
    @EventHandler
    public void handle(InventoryClickEvent e) {
        data.lookup(e.getInventory()).ifPresent(InventoryContainer::updateContents);
    }
}
