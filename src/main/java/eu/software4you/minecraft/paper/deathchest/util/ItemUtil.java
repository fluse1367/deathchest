package eu.software4you.minecraft.paper.deathchest.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import eu.software4you.reflect.Parameter;
import eu.software4you.reflect.ReflectUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Base64;
import java.util.UUID;

public class ItemUtil {
    public static ItemStack getSkull(String textures) {
        ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1);
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();

        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", textures));

        ReflectUtil.forceCall(skullMeta.getClass(), skullMeta, "profile", Parameter.guessMultiple(profile));
        skull.setItemMeta(skullMeta);
        return skull;
    }

    public static GameProfile genSkullProfile(UUID uuid, String url) {
        GameProfile profile = new GameProfile(uuid, null);
        Property prop = new Property("textures", Base64.getEncoder().encodeToString(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes()));
        profile.getProperties().put("textures", prop);
        return profile;
    }
}
